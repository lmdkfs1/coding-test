FROM nicolaka/netshoot:latest
ADD demo /data/demo

EXPOSE 8080
WORKDIR /data

ENTRYPOINT ["./demo", "start"]
