package cmd

import (
	"coding-test/config"
	"coding-test/pkg/server"
	"github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"log"
	"os"
	"fmt"
	"strings"
)

var cfgFile string
var cfg = config.NewConfig()

var rootCmd = &cobra.Command{
	Use:   "start",
	Short: "start",
	Long:  "start",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Start server")
		server.Start()
	},
}

// Execute ...
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.cfg.yaml)")
	rootCmd.PersistentFlags().StringP("port", "p", "8888", "listen port")
	rootCmd.PersistentFlags().StringP("host","s", "0.0.0.0","listen host")

	viper.BindPFlag("server.port", rootCmd.PersistentFlags().Lookup("port"))
	viper.BindPFlag("server.host", rootCmd.PersistentFlags().Lookup("host"))

}

func initConfig() {

	currentDir, err := os.Getwd()
	if err != nil {
		log.Panicf("Get currentir Fail: %s ", err.Error())
	}
	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		home, err := homedir.Dir()
		if err != nil {
			log.Printf("Get $HOME Dir error:%s", err.Error())
			os.Exit(1)
		}
		viper.AddConfigPath(currentDir)
		viper.AddConfigPath(home)
		viper.SetConfigName(".cfg")
	}

	// Search config in home directory with name ".config" (without extension)

	viper.SetEnvPrefix("codingtest")                       // 环境变量前缀,环境变量必须大写.
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_")) // 为了兼容读取yaml文
	viper.AutomaticEnv()
	viper.SetDefault("server.host", "0.0.0.0")
	viper.SetDefault("server.port", "8080")
	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		log.Println("Using config file:", viper.ConfigFileUsed())

	}

	cfg.RunMode = viper.GetString("server.run_mode")
	cfg.HTTP.Host = viper.GetString("server.host")
	cfg.HTTP.Port = viper.GetInt("server.port")
}
