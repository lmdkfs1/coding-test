package config

import (
	"sync"
)

var (
	global *Config
	once   sync.Once
)

func NewConfig() *Config {
	once.Do(func() {
		global = &Config{}
	})
	return global
}

type Config struct {
	RunMode string
	HTTP    HTTP
}

type HTTP struct {
	Host            string
	Port            int
	ShutdownTimeout int
}
