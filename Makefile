PROJECT="demo"
BINARY="demo"
VERSION=0.0.1
default:
	go build -o ${BINARY} main.go
linux:
	GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o demo main.go
test:
	go test -v -coverprofile=cover.out ./pkg/server/ 
	go tool cover -html=cover.out -o test.html

docker:
	docker build -t ${PROJECT}:${VERSION} .
clean:
	cd bin && if [ -f ${BINARY} ] ; then rm  ${BINARY} ; fi

